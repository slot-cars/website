---
title: "Electronic Speed Controllers for Brushless DC Slot Cars"
---

# Electronic Speed Controllers For Brushless DC Slot Car Motors

This site is a resource for information about Brushless DC Slot Cars.

It's mainly about electronic speed controllers (ESC) and motors, which is where our current interest lies and what has dominated our research.

Here you'll find a lot of background informaiton that we gathered as well as links to other resources that we hope you'll find useful.

Our goal is to develop a competetive and inexpensive ESC that is completely open source, so that anyone can create one for themselves and to encourage collaboration.

Slot car racing started out as a hobby that was relatively easy to get started with. It took a few basic skills and some understanding of electricity. It provided a platform for learning some very valuable basic technical skills. That leaning could move into deeper understanding of the laws of physics and mechanical and electrical engineering.

Unfortunately to do well in the hobby it now requires a relatively large budget to spend on motors, controllers, and chassis. It's no longer easy to be a hobbyist with some piano wire and a soldering iron.

One of the biggest outlays is a motor. Motors are conventional brushed DC motors, that wear out. Cheap, production motors are about $12-$15 pop and can't be repaired. More exhotic "strap" motors are typically $150, can be repaired, but must be maintained frequently to remain competetive.

Within the last couple of years people have begun to experiment with minature brushless DC motors - of the sort used in small drones. They relatively inexpensive, at around $15. They should last longer and require less maintenance since they do not have brushes.

## Topics Covered

* Motor theory
* Important capabilities of motors for slot car racing
* A survey of the available BLDC's
* Slot car ESC's
* Theory of ESC's
* Microcontrollers for slot car ESC's
* ESC boards
* How to develop you own slot car ESC
* Lots of references to resources abou the subject
* Areas for future work

* [Automatic Search]({{%relref "basics/configuration/_index.md#activate-search" %}})
* [Multilingual mode]({{%relref "cont/i18n/_index.md" %}})
* **Unlimited menu levels**
* **Automatic next/prev buttons to navigate through menu entries**
* [Image resizing, shadow...]({{%relref "cont/markdown.en.md#images" %}})
* [Attachments files]({{%relref "shortcodes/attachments.en.md" %}})
* [List child pages]({{%relref "shortcodes/children/_index.md" %}})
* [Mermaid diagram]({{%relref "shortcodes/mermaid.en.md" %}}) (flowchart, sequence, gantt)
* [Customizable look and feel and themes variants]({{%relref "basics/style-customization/_index.md"%}})
* [Buttons]({{%relref "shortcodes/button.en.md" %}}), [Tip/Note/Info/Warning boxes]({{%relref "shortcodes/notice.en.md" %}}), [Expand]({{%relref "shortcodes/expand.en.md" %}})

![Screenshot](https://github.com/matcornic/hugo-theme-learn/raw/master/images/screenshot.png?width=40pc&classes=shadow)

## Contribute to this documentation
Feel free to update this content, just click the **Edit this page** link displayed on top right of each page, and pullrequest it

{{% notice info %}}
Your modification will be deployed automatically when merged.
{{% /notice %}}

## Documentation website
This current documentation has been statically generated with Hugo with a simple command : `hugo -t hugo-theme-learn` -- source code is [available here at GitHub](https://github.com/matcornic/hugo-theme-learn)

{{% notice note %}}
Automatically published and hosted thanks to [Netlify](https://www.netlify.com/). Read more about [Automated HUGO deployments with Netlify](https://www.netlify.com/blog/2015/07/30/hosting-hugo-on-netlifyinsanely-fast-deploys/)
{{% /notice %}}